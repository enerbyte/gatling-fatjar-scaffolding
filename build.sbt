import sbt._
import Keys._

name := "gatling-poc"

version := "0.9.2-SNAPSHOT"

scalaVersion := "2.11.7"


libraryDependencies ++=
  Seq(
    "io.gatling" % "gatling-core" % "2.2.2",
    "io.gatling" % "gatling-app" % "2.2.2",
    "io.gatling" % "gatling-charts" % "2.2.2",
    "io.gatling.highcharts" % "gatling-charts-highcharts" % "2.2.2",
    "io.gatling" % "gatling-http" % "2.2.2"
  )

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", x) if Seq("BCKEY.DSA", "BCKEY.SF", "SAXONICA.RSA", "SAXONICA.SF").contains(x) => MergeStrategy.discard
  case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
  case PathList("META-INF", "io.netty.versions.properties") => MergeStrategy.discard
  case _ => MergeStrategy.last
}


mainClass in assembly := Some("com.enerbyte.MainSimulation")
